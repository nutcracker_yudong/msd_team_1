from app import example_for_test, welcome

# because we use sessionStorage, the pytest always works out of scope


def test_example():
    assert example_for_test(1, 2) == 3


def test_welcome():
    assert welcome() == "Welcome to the MSD-TEAM-1 library --deployed by CircleCI<br /><br />/start to load a new " \
                        "library <br /><br />/add to add a book <br /><br />"

