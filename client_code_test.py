import requests

debug_flag = True

# url = 'https://msd-team-1.herokuapp.com'
url = 'http://127.0.0.1:5000'


# test '/users' ['GET']
resp = requests.get(url + '/users')
user_yudong = resp.json()[0]
if debug_flag:
    print(user_yudong)
expect_res_user = {'email': 'wangyudong53138@gmail.com', 'id': 1, 'name': 'yudong', 'password': '123',
                   'user_name': 'rexue70'}
assert user_yudong == expect_res_user
print("pass: /users ['GET']")


# test '/users' ['POST']
test_client_user = {
    'email': 'client_test@gmail.com',
    'name': 'client_test',
    'password': '123',
    'user_name': 'client_test'
}
if debug_flag:
    print(test_client_user)
headers = {"Content-type": "application/json"}
resp = requests.post(url + '/users', headers=headers, json=test_client_user)

print("yudong" + resp)
assert resp.json()['message'] == 'successfully added'
test_client_user_id = resp.json()['user']['id']
print("pass: /users ['POST']")


# test 'users/<user_id>' ['GET']
resp = requests.get(url + '/users/' + str(test_client_user_id))
user_yudong = resp.json()
if debug_flag:
    print(user_yudong)
expect_client_user = {
    'email': 'client_test@gmail.com',
    'name': 'client_test',
    'password': '123',
    'user_name': 'client_test',
    'id':test_client_user_id
}
assert user_yudong == expect_client_user
print("pass: /users/<user_id> ['GET']")


# test '/users' ['PUT']
test_client_user = {
    'name': 'client_test_put',
    'password': '321',
}
if debug_flag:
    print(test_client_user)
headers = {"Content-type": "application/json"}
resp = requests.put(url + '/users/' + str(test_client_user_id), headers=headers, json=test_client_user)
if debug_flag:
    print(resp.text)
assert resp.json() == 'successfully updated'
print("pass: /users/<user_id> ['PUT']")


# test 'users/<user_id>' ['DELETE']
resp = requests.delete(url + '/users/' + str(test_client_user_id))
user_yudong = resp.json()
if debug_flag:
    print(user_yudong)
assert user_yudong == "successfully deleted"
print("pass: /users/<user_id> ['DELETE']")