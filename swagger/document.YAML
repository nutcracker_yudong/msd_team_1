openapi: 3.0.0
info:
  version: "1.1"
  title: MSD-TEAM-1 Library-Document
  description: |
    This is MSD-TEAM-1 swaggerhub documentation.
    Team member are Yudong Wang, Xiwen Song
servers:
# Added by API Auto Mocking Plugin
  - description: SwaggerHub API Auto Mocking
    url: https://msd-team-1.herokuapp.com/

components:
  schemas:
    User:
      properties:
        email:
          type: string
        name:
          type: string
        user_name:
          type: string
        password:
          type: string
    Login:
      properties:
        username:
          type: string
        password:
          type: string
    Book:
      properties:
        id:
          type: integer
        name:
          type: string
        author:
          type: string
        subject:
          type: string
        time_period:
          type: string
        notes:
          type: string
        borrower_name:
          type: string
        return_date:
          type: string
        overdue:
          type: boolean
    Note:
      properties:
        note:
          type: string
    Filter:
      properties:
        author:
          type: string
        time_period_start:
          type: string
        time_period_end:
          type: string
        subject:
          type: string
    Collection:
      properties:
        collection_name:
          type: string


  responses:
    ParseError:
      description: When a schema filed can not be Parsed
    MaskError:
      description: When mask has any error

tags:
  - name: User
    description: APIs for User operations
  - name: Book
    description: APIs for Books operations
  - name: Note
    description: APIs for Notes operations
  - name: Collection
    description: APIs for favourite collection operations
  - name: Search
    description: APTs for Search operations
  - name: Remind
    description: Send email reminder for uesr to return the book
  - name: Inventory
    description: Show all current book borrow/loan status record
  - name: User-Books
    description: User borrowed books


paths:
  /login:
    post:
      summary: login with a username and password
      operationId: userLogin
      tags:
        - Login
      requestBody:
        required: True
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Login"
      responses:
        '200':
          description: You are successfully login with valid information
        '401':
          description: It is not a valid username password match


  /users:
    get:
      summary: return list of all users
      operationId: getAllUsers
      tags:
        - User
      responses:
        '200':
          description: This is a json file contains of all the users
    post:
      summary: create a new user
      operationId: createUser
      tags:
        - User
      requestBody:
        required: True
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"
      responses:
        '201':
          description: User created successfully
        '409':
          description: There already exists such user
  /users/{user_id}:
    get:
      summary: return a user based on the given id
      operationId: getUserById
      tags:
        - User
      parameters:
        - name: user_id
          in: path
          description: id of user
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '404':
          description: User not in the database

    put:
      summary: update a user based on userID an replaced with new user object
      operationId: updateUserById
      tags:
        - User
      parameters:
        - name: user_id
          in: path
          description: id of user
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully update the user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '404':
          description: User not in the database, can not update non-exist user

    delete:
      summary: delete a user based on given id
      operationId: deleteUserById
      tags:
        - User
      parameters:
        - name: user_id
          in: path
          description: id of user
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully delete a user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '404':
          description: User not in the database, can not delete non-exist user

  /users/{user_id}/books:
    get:
      summary: get a list of books borrowed by the user
      operationId: getBooksByUser
      tags:
        - User-Books
      parameters:
        - name: user_id
          in: path
          required: true
          description: The user ID
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Get all books successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'

  /users/{user_id}/books/{book_id}:
    get:
      summary: get a book detail information borrowed by the user
      operationId: getBookByUserIdAndBookId
      tags:
        - User-Books
      parameters:
        - name: user_id
          in: path
          required: true
          description: The user ID
          schema:
            type: integer
            format: int64
        - name: book_id
          in: path
          required: true
          description: The book ID
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Get book by userId and bookId successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
    post:
      summary: (important) borrow a book by bookid and userid
      operationId: updateBookByUserIdAndBookId
      tags:
        - User-Books
      parameters:
        - name: user_id
          in: path
          required: true
          description: The user ID
          schema:
            type: integer
            format: int64
        - name: book_id
          in: path
          required: true
          description: The book ID
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Borrow book successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
    put:
      summary: （important) return a book borrowed by the user by book id
      operationId: returnBookByUserIdAndBookId
      tags:
        - User-Books
      parameters:
        - name: user_id
          in: path
          required: true
          description: The user ID
          schema:
            type: integer
            format: int64
        - name: book_id
          in: path
          required: true
          description: The book ID
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: return book successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'

  /books:
    get:
      summary: return all the books owned by library
      operationId: getAllBooks
      tags:
        - Book
      responses:
        '200':
          description: A JSON array of all Books
    post:
      summary: add a new book to the library
      operationId: createBook
      tags:
        - Book
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Book'
      responses:
        '201':
          description: Create book successfully
        '409':
          description: Book already exists
        '500':
          description: Internal server error, sorry we don't know what's happened

  /books/{book_id}:
    get:
      summary: get the book from library based on the book id
      operationId: getBookById
      tags:
        - Book
      parameters:
        - name: book_id
          in: path
          description: Book ID
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: success get the book
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
        '404':
          description: book not in the database, can not find the book with given book id
    put:
      summary: update the book from library based on the book id
      operationId: updateBookById
      tags:
        - Book
      parameters:
        - name: book_id
          in: path
          description: Book ID
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: success update the book
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
        '404':
          description: book not in the database, can not update the book with given book id
    delete:
      summary: delete the book from library based on the book id
      operationId: deleteBookById
      tags:
        - Book
      parameters:
        - name: book_id
          in: path
          description: Book ID
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: success delete the book
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
        '404':
          description: book not in the database, no need to delete

  /books/{book_id}/notes:
    get:
      summary: get all notes of a book by book id
      operationId: getNotesByBookId
      tags:
        - Note
      parameters:
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully get the notes by book id
        '404':
          description: can not find the notes based on book id
    post:
      summary: create a new note to the book
      operationId: createNoteByBookId
      tags:
        - Note
      requestBody:
        required: True
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Note"
      parameters:
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully add the note to book
        '404':
          description: can not find the book by book id, can not add notes


  /books/{book_id}/notes/{note_id}:
    get:
      summary: get a note of a book by note id and book id
      operationId: getNoteByBookIdAndNoteId
      tags:
        - Note
      parameters:
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
        - name: note_id
          in: path
          description: note id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully get the note by book id and note id
        '404':
          description: can not find book id or note id

    put:
      summary: update a note of a book by note id and book id
      operationId: updateNoteByBookIdAndNoteId
      tags:
        - Note
      requestBody:
        required: True
        content:
          application/json:
            schema:
              type: string
      parameters:
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
        - name: note_id
          in: path
          description: note id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully update the note by book id and note id
        '404':
          description: can not find book id or note id
    delete:
      summary: delete a note of a book by note id and book id
      operationId: deleteNoteByBookIdAndNoteId
      tags:
        - Note
      parameters:
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
        - name: note_id
          in: path
          description: note id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully delete the note by book id and note id
        '404':
          description: can not find book id or note id

  /search:
    post:
      summary: search books based on author, subject and time_period
      operationId: searchBooksByFilter
      tags:
        - Search
      requestBody:
        required: True
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Filter'
      responses:
        '200':
          description: successfully searched books based on filter
        '404':
          description: can not find the book based on filter, maybe you use the wrong filter format

  /remind:
    get:
      summary: alert the user who has book overdue the return date
      operationId: alert
      tags:
        - Remind
      responses:
        '200':
          description: we successfully alert the user who has book overdue the return date

  /users/{user_id}/collections:
    get:
      summary: get all collections of user by user id
      operationId: getCollectionsByUserId
      tags:
        - Collection
      parameters:
        - name: user_id
          in: path
          description: user id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully get the collections by user id
        '404':
          description: can not find the collections based on user id
    post:
      summary: create new empty collection list
      operationId: createCollection
      tags:
        - Collection
      requestBody:
        required: True
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Collection'
      parameters:
        - name: user_id
          in: path
          description: user id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully create new collection list
        '404':
          description: can not find the user id

  /users/{user_id}/collections/{collection_name}:
    get:
      summary: get collection of user by user id and collection name
      operationId: getCollectionByUserIdAndCollectionId
      tags:
        - Collection
      parameters:
        - name: user_id
          in: path
          description: user id
          required: true
          schema:
            type: integer
            format: int64
        - name: collection_name
          in: path
          description: collection name
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successfully get the collection by user id and colelction name
        '404':
          description: can not find the collection based on user id and collection name

  /users/{user_id}/collections/{collection_name}/books/{book_id}:
    post:
      summary: add a book to the user's collection
      operationId: addBookToCollection
      tags:
        - Collection
      parameters:
        - name: user_id
          in: path
          description: user id
          required: true
          schema:
            type: integer
            format: int64
        - name: collection_name
          in: path
          description: collection name
          required: true
          schema:
            type: string
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully add a book to collection (collection_name)
        '404':
          description: there is no such book in the collection
    get:
      summary: get a book detail in a user's collection
      operationId: getBookDetailFromCollection
      tags:
        - Collection
      parameters:
        - name: user_id
          in: path
          description: user id
          required: true
          schema:
            type: integer
            format: int64
        - name: collection_name
          in: path
          description: collection name
          required: true
          schema:
            type: string
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully delete the book from collection
        '404':
          description: there is no such book in the collection
    delete:
      summary: delte book from collection
      operationId: deleteBookFromCollection
      tags:
        - Collection
      parameters:
        - name: user_id
          in: path
          description: user id
          required: true
          schema:
            type: integer
            format: int64
        - name: collection_name
          in: path
          description: collection name
          required: true
          schema:
            type: string
        - name: book_id
          in: path
          description: book id
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: successfully delete the book from collection
        '404':
          description: there is no such book in the collection

  /inventory:
      get:
        summary: return list of all current borrow book record
        operationId: getAllBorrowRecord
        tags:
          - Inventory
        responses:
          '200':
            description: This is a json file contains of all the inventory