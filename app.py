from datetime import datetime, timedelta

from flask import Flask, request, session, json, jsonify
import smtplib
from flask_cors import CORS
from email_validator import validate_email, EmailNotValidError
import connexion


app = Flask(__name__)
CORS(app)
app.secret_key = 'MSD NEU Team One'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://hrwvzbsqhnjxtc:48b88e685797bf23c46d435e5828a27ef93d37f7b299f35c1c' \
                                        '4829b0c3f76561@ec2-54-204-21-226.compute-1.amazonaws.com:5432/ddvbm8t42lpae9'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test3.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#
# app = connexion.App(__name__, specification_dir='swagger/')
# app.add_api('document.YAML')
# app.run(port=8080)


from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import inspect

db = SQLAlchemy(app)
migrate = Migrate(app, db)



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=False)
    email = db.Column(db.String(128), unique=False)
    user_name = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(60), unique=False, nullable=False)

    def __init__(self, name, email, user_name, password):
        self.name = name
        self.email = email
        self.user_name = user_name
        self.password = password

    def __repr__(self):
        return str(self.to_dict())

    def to_dict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=False, nullable=False)
    author = db.Column(db.String(40), unique=False, nullable=False)
    subject = db.Column(db.String(128), unique=False, nullable=False)
    publish_date = db.Column(db.String(128), unique=False, nullable=False)
    available = db.Column(db.Boolean, unique=False, nullable=False)

    def __init__(self, name, author, subject, publish_date):
        self.name = name
        self.author = author
        self.subject = subject
        self.publish_date = publish_date
        self.available = True

    def __repr__(self):
        return str(self.to_dict())

    def to_dict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}


class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, unique=False, nullable=False)
    book_id = db.Column(db.Integer, unique=False, nullable=False)
    is_returned = db.Column(db.Boolean, unique=False, nullable=False)
    overdue = db.Column(db.Boolean, unique=False, nullable=False)
    borrow_date = db.Column(db.TIMESTAMP, unique=False, nullable=False)
    return_date = db.Column(db.TIMESTAMP, unique=False, nullable=False)

    def __init__(self, user_id, book_id, borrow_date, return_date):
        self.user_id = user_id
        self.book_id = book_id
        self.borrow_date = borrow_date
        self.return_date = return_date
        self.is_returned = False
        self.overdue = False

    def __repr__(self):
        return str(self.to_dict())

    def to_dict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}


class Collection(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, unique=False, nullable=False)
    book_id = db.Column(db.Integer, unique=False, nullable=False)
    collection_name = db.Column(db.String(128), unique=False, nullable=False)

    def __init__(self, user_id, book_id, collection_name):
        self.user_id = user_id
        self.book_id = book_id
        self.collection_name = collection_name

    def __repr__(self):
        return str(self.to_dict())

    def to_dict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    book_id = db.Column(db.Integer, unique=False, nullable=False)
    note = db.Column(db.String(512), unique=False, nullable=False)

    def __init__(self, book_id, note):
        self.book_id = book_id
        self.note = note

    def __repr__(self):
        return str(self.to_dict())

    def to_dict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}


@app.route("/")
def welcome():
    text = "33i Welcome to the MSD-TEAM-1 library --deployed by CircleCI<br /><br />" \
           "/user/:user_id/books <br /><br /> to get list of books <br /><br />" \
           "/user/:user_id/books/:book_id/ <br /><br /> to get/update/delete one book <br /><br />"
    return text


@app.route("/login", methods=['POST'])
def login():
    if request.method == 'POST':
        username = request.json['username']
        password = request.json['password']
        backend_user = User.query.filter_by(user_name=username).first()
        if backend_user:
            if backend_user.password == password:
                resp = jsonify({'message': 'successfully login'})
                resp.status_code = 200
                return resp
            else:
                resp = jsonify({'message': 'It is not a valid username password match'})
                resp.status_code = 401
                return resp
        else:
            resp = jsonify({'message': 'We do not find the username you are giving'})
            resp.status_code = 401
            return resp


@app.route("/users", methods=['POST', 'GET'])
def users():
    users = User.query.all()
    if request.method == 'GET':
        print(users)
        usersArr = []
        for user in users:
            usersArr.append(user.to_dict())
        return jsonify(usersArr)

    if request.method == 'POST':
        user = request.json
        print("yudong========")
        print(user)
        new_user = User(name=user['name'], email=user['email'], user_name=user['user_name'], password=user['password'])
        print(new_user)

        for exist_user in users:
            if exist_user.user_name == new_user.user_name:
                resp = jsonify("Duplicate user_name error! You must provide unique user_name")
                resp.status_code = 409
                return resp

        db.session.add(new_user)
        db.session.commit()
        user_copy = User.query.filter_by(user_name=user['user_name']).first()
        resp = jsonify({'message':'successfully added', 'user':user_copy.to_dict()})
        resp.status_code = 200
        return resp


@app.route("/users/<user_id>", methods=['GET', 'DELETE', 'PUT'])
def user(user_id):
    print("user_id = " + user_id)
    user = User.query.filter_by(id=user_id).first()
    print(user)
    if request.method == 'GET':
        if user:
            print("find it!")
            print(user)
            resp = jsonify(user.to_dict())
            resp.status_code = 200
            return resp
        else:
            print("not found")
            return not_found()
    if request.method == 'DELETE':
        print("in the delete")
        if user:
            print("in the print delete")
            db.session.delete(user)
            db.session.commit()
            resp = jsonify("successfully deleted")
            resp.status_code = 200
            return resp
        else:
            return not_found()
    if request.method == 'PUT':
        # check valid user format
        if user:
            new_user = request.json
            user.name = new_user['name']
            user.password = new_user['password']
            user.email = new_user['email']

            print("yudong========")
            print(user)
            db.session.commit()

            resp = jsonify("successfully updated")
            resp.status_code = 200
            return resp
        else:
            return not_found()


@app.route("/books", methods=['POST', 'GET'])
def library_books():
    print("we are in the right search")
    print("==========")
    books = Book.query.all()
    if request.method == 'GET':
        print(books)
        booksArr = []
        for book in books:
            booksArr.append(book.to_dict())
        return jsonify(booksArr)

    if request.method == 'POST':
        if not request.json or 'name' not in request.json or 'author' not in request.json or \
                'subject' not in request.json or 'publish_date' not in request.json:
            return "Add Failed. You book lack filed of necessary field(name, author, subject, publish_time)"
        book = request.json
        print("yudong========")
        print(book)
        new_book = Book(name=book['name'], author=book['author'], subject=book['subject'],
                        publish_date=book['publish_date'])
        print(new_book)
        db.session.add(new_book)
        db.session.commit()

        new_book_copy = Book.query.filter_by(name=book['name']).first()
        return jsonify(new_book_copy.to_dict())


@app.route("/books/<book_id>", methods=['GET', 'DELETE', 'PUT'])
def library_book(book_id):
    print("book_id = " + book_id)
    book = Book.query.filter_by(id=book_id).first()
    print(book)
    if request.method == 'GET':
        if book:
            print("find it!")
            print(book)
            resp = jsonify(book.to_dict())
            resp.status_code = 200
            return resp
        else:
            print("not found")
            return not_found()
    if request.method == 'DELETE':
        print("in the delete")
        if book:
            print("in the print delete")
            db.session.delete(book)
            db.session.commit()
            resp = jsonify("successfully deleted")
            resp.status_code = 200
            return resp
        else:
            return not_found()
    if request.method == 'PUT':
        # check valid book format
        if book:
            new_book = request.json

            book.name = new_book['name']
            book.author = new_book['author']
            book.subject = new_book['subject']
            book.publish_date = new_book['publish_date']
            # book.notes = new_book['notes']

            print("yudong========")
            print(book)
            db.session.commit()

            resp = jsonify("successfully updated")
            resp.status_code = 200
            return resp
        else:
            return not_found()


@app.route("/books/<book_id>/notes", methods=['POST', 'GET'])
def book_notes(book_id):
    print("we are in the notes")
    print("==========")

    if request.method == 'GET':
        print("Get list of notes of that book")
        notes = Note.query.filter_by(book_id=book_id).all()
        notes_arr = []
        for note in notes:
            notes_arr.append(note.to_dict())
        return jsonify(notes_arr)

    if request.method == 'POST':
        if not request.json or 'note' not in request.json:
            return "Add Failed. You note lack filed of necessary field(note)"
        note = request.json['note']
        print("yudong========")
        print(note)
        new_note = Note(book_id=book_id, note=note)

        print(new_note)
        db.session.add(new_note)
        db.session.commit()
        resp = jsonify("successfully added new note")
        resp.status_code = 200
        return resp


@app.route("/books/<book_id>/notes/<note_id>", methods=['GET', 'DELETE', 'PUT'])
def book_note(book_id, note_id):
    print("book_id = " + book_id + " note_id = " + note_id)
    note = Note.query.filter_by(id=note_id).first()
    print(note)
    if request.method == 'GET':
        if note:
            print("find it!")
            print(note)
            resp = jsonify(note.to_dict())
            resp.status_code = 200
            return resp
        else:
            print("not found this note")
            return not_found()
    if request.method == 'DELETE':
        print("in the delete")
        if note:
            print("in the print delete")
            db.session.delete(note)
            db.session.commit()
            resp = jsonify("successfully deleted")
            resp.status_code = 200
            return resp
        else:
            return not_found()
    if request.method == 'PUT':
        if not request.json or 'note' not in request.json:
            return "Add Failed. You note lack filed of necessary field(note)"
        if note:
            new_note = request.json
            note.note = new_note['note']

            print("yudong========")
            print(note)
            db.session.commit()

            resp = jsonify("successfully updated")
            resp.status_code = 200
            return resp
        else:
            return not_found()


@app.route('/users/<user_id>/books', methods=['GET'])
def books(user_id):
    print("get list of books borrowed by user")
    if request.method == "GET":
        links = Link.query.filter_by(user_id=user_id).all()
        print("===========links============")
        print(links)
        booksArr = []
        for link in links:
            print(link.is_returned)
            if not link.is_returned:
                book = Book.query.filter_by(id=link.book_id).first()
                if book:
                    booksArr.append({
                        "borrow_record": link.to_dict(),
                        "book_detail": book.to_dict()
                    })
        return jsonify(booksArr)
    else:
        return invalid_url()


@app.route('/users/<user_id>/books/<book_id>', methods=['GET', 'PUT', 'POST'])
def book(user_id, book_id):
    # put is for return book
    if request.method == 'PUT':

        link = Link.query.filter_by(user_id=user_id, book_id=book_id, is_returned=False).first()
        if link:
            link.is_returned = True
            if datetime.today() > link.return_date:
                print("it is overdue")
                link.overdue = True
            else:
                print("is returned within date, not overdue")
                link.overdue = False

            book = Book.query.filter_by(id=book_id).first()
            book.available = True
            db.session.commit()
            return "successfully return the book, your overdue status is = " + str(link.overdue)
        else:
            resp = jsonify("This book doesn't borrowed to you")
            resp.status_code = 404
            return resp

    if request.method == "GET":
        link = Link.query.filter_by(user_id=user_id, book_id=book_id, is_returned=False).first()
        if link:
            resp = {
                "borrow_record": link.to_dict(),
                "book_detail": Book.query.filter_by(id=link.book_id).first().to_dict()
            }
            return jsonify(resp)
        else:
            resp = jsonify("This book doesn't borrowed to you")
            resp.status_code = 404
            return resp

    # post is for borrow book
    if request.method == "POST":
        book = Book.query.filter_by(id=book_id).first()
        if not book.available:
            resp = jsonify("This book is not available, you can not borrow it.")
            resp.status_code = 404
            return resp
        else:
            borrow_date = datetime.today()
            return_date = datetime.today() + timedelta(days=10)
            link = Link(user_id=user_id, book_id=book_id, borrow_date=borrow_date, return_date=return_date)
            db.session.add(link)
            book.available = False
            db.session.commit()
            resp = jsonify("successfully borrowed the book")
            resp.status_code = 200
            return resp


@app.route('/search', methods=['POST'])
def search():
    if not request.json:
        return "Search Failed. You search lack necessary request field(author, subject, time_period_start, " \
               "time_period_end)"
    filter = request.json
    books = Book.query.all()
    print("new filter is " + str(filter))

    if filter.get('author'):
        author = filter['author']
        booksArr = []
        for book in books:
            if book.author == author:
                booksArr.append(book)
        books = booksArr
        print("finish author filter")
        print(books)

    if filter.get('subject'):
        subject = filter['subject']
        booksArr = []
        for book in books:
            if book.subject == subject:
                booksArr.append(book)
        books = booksArr
        print("finish subject filter")
        print(books)

    if filter.get('time_period_start') and filter.get('time_period_end'):
        time_period_start, time_period_end = filter['time_period_start'], filter['time_period_end']

        start_year, start_month, start_day = filter['time_period_start'].split("-")
        end_year, end_month, end_day = filter['time_period_end'].split("-")

        booksArr = []
        for book in books:
            if "-" in book.publish_date:
                year, month, day = book.publish_date.split("-")
                if int(start_year) * 365 + int(start_month) * 31 + int(start_day) \
                        <= int(year) * 365 + int(month) * 31 + int(day) \
                        <= int(end_year) * 365 + int(end_month) * 31 + int(end_day):
                    booksArr.append(book)
        books = booksArr
        print("finish publish_date filter")
        print(books)

    booksArr = []
    for book in books:
        booksArr.append(book.to_dict())
    return jsonify(booksArr)


@app.route('/users/<user_id>/collections', methods=['GET', 'POST', 'DELETE'])
def collections(user_id):
    print("get list of collections by user")
    if request.method == "GET":
        collections = Collection.query.filter_by(user_id=user_id).all()
        print("===========collections============")
        print(collections)
        collections_name_set = set([])
        for c in collections:
            collections_name_set.add(c.collection_name)
        print("finish set add collection name")
        collections_res = []
        for name in collections_name_set:
            collections_res.append(name)
        return jsonify(collections_res)
    if request.method == "POST":
        if not request.json or 'collection_name' not in request.json:
            return "Create collection Failed. You should provide a collection name"
        collection_name = request.json['collection_name']
        collection = Collection(user_id=user_id, book_id=-1, collection_name=collection_name)
        db.session.add(collection)
        db.session.commit()
        resp = jsonify("successfully created collection")
        resp.status_code = 200
        return resp
    if request.method == "DELETE":
        if not request.json or 'collection_name' not in request.json:
            return "Delete collection Failed. You should provide a collection name"
        collection_name = request.json['collection_name']

        to_remove_collections = Collection.query.filter_by(user_id=user_id, collection_name=collection_name)
        for c in to_remove_collections:
            db.session.delete(c)
        db.session.commit()
        resp = jsonify("successfully delete collection")
        resp.status_code = 200
        return resp


@app.route('/users/<user_id>/collections/<collection_name>', methods=['GET'])
def get_collection_by_name(user_id, collection_name):
    print("get a collection by collection_name")
    print(collection_name)
    if request.method == "GET":
        collections = Collection.query.filter_by(user_id=user_id, collection_name=collection_name).all()
        print("===========collections============")
        print(collections)
        collections_res = []
        for c in collections:
            if c.book_id != -1:
                collections_res.append(
                    Book.query.filter_by(id=c.book_id).first().to_dict()
                )
        return jsonify(collections_res)
    else:
        return not_found()


@app.route('/users/<user_id>/collections/<collection_name>/books/<book_id>', methods=['GET', 'POST', 'DELETE'])
def add_delete_collection_book(user_id, collection_name, book_id):
    print("add_delete_collection_book")
    print(collection_name)
    print(book_id)

    if request.method == "GET":
        book = Collection.query.filter_by(user_id=user_id, collection_name=collection_name, book_id=book_id).first()
        if book:
            print("===========collections============")
            print(book.to_dict())
            return jsonify(book.to_dict())
        else:
            return "the book you are looking for is not exist in this collection"

    if request.method == "POST":
        new_collection_record = Collection(user_id=user_id, book_id=book_id, collection_name=collection_name)

        print("===========collections============")

        db.session.add(new_collection_record)
        db.session.commit()
        resp = jsonify("successfully add a book to collection (" + collection_name + ")")
        resp.status_code = 200
        return resp

    if request.method == "DELETE":

        to_remove_collections = Collection.query.filter_by(user_id=user_id, book_id=book_id,
                                                           collection_name=collection_name)
        if to_remove_collections:
            for c in to_remove_collections:
                db.session.delete(c)
            db.session.commit()
            resp = jsonify("successfully delete collection")
            resp.status_code = 200
            return resp
        else:
            return "the book is not in the collections"


@app.route('/inventory', methods=['GET'])
def check_borrow_inventory():
    if request.method == "GET":
        links = Link.query.all()
        print("===========links============")
        print(links)
        links_res = []
        for link in links:

            book = Book.query.filter_by(id=link.book_id).first()
            # print("======book=======")
            # print(book)

            if book:
                links_res.append(
                    {
                        "book": book.to_dict(),
                        "loaned_to_user": User.query.filter_by(id=link.user_id).first().to_dict(),
                        "detail": link.to_dict()
                    }
                )
        return jsonify(links_res)


@app.route('/remind', methods=['GET'])
def remind_to_return_book():
    if request.method == "GET":
        links = Link.query.all()
        print("===========links============")
        print(links)
        links_res = []
        for link in links:
            if not link.is_returned:
                print("debug")
                print(link.to_dict())
                print("debug======")
                print(Book.query.filter_by(id=link.book_id).first())
                if Book.query.filter_by(id=link.book_id).first():
                    email = User.query.filter_by(id=link.user_id).first().email
                    try:
                        v = validate_email(email)  # validate and get info
                        email = v["email"]  # replace with normalized form
                        send_email_reminder(User.query.filter_by(id=link.user_id).first().email, link.return_date,
                        Book.query.filter_by(id=link.book_id).first().name)
                        links_res.append(link.to_dict())
                    except EmailNotValidError as e:
                        # email is not valid, exception message is human-readable
                        print(str(e))
                        print("invalid email format")

                        resp = jsonify({"message": "invalid email address format " + str(e),"email":email})
                        resp.status_code = 404
                        return resp


        resp = jsonify({"message": "successfully remind all the users", "remind_list": links_res})
        resp.status_code = 200
        return resp


def send_email_reminder(email, return_date, book_name):
    print("inside email " + email)



    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login("wangyudong53138@gmail.com", "HAncuiyan_1968")

    date = return_date.strftime("%A %d. %B %Y")
    print(date)
    msg = "You need to return the book name = " + book_name + " by the date = " + date
    print(msg)

    server.sendmail("wangyudong53138@gmail.com", email, str(msg))
    server.quit()
    pass


@app.errorhandler(404)
def invalid_url(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@app.errorhandler(400)
def not_found(error=None):
    message = {
        'status': 400,
        'message': "the element you are looking for is not exist"
    }
    resp = jsonify(message)
    resp.status_code = 400
    return resp


def example_for_test(a, b):
    return a + b


if __name__ == "__main__":
    app.run(debug=True)
